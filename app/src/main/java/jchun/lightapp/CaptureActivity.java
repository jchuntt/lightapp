package jchun.lightapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.UUID;


public class CaptureActivity extends Activity {

    private static final int REQUEST_CAPTURE_QR = 1;
    private static final int REQUEST_CAPTURE_RESULTS = 2;

    private static final String POST_UPLOAD_URL =
            "https://light-dev.r.staging.knewton.com/offline/upload/app";

    private static final String KEY_LEARNING_OBJECTIVE_ID = "learningObjectiveId";
    private static final String KEY_SEQUENCE_ID = "sequenceId";

    private ImageView resultImageView;

    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture);

        if (savedInstanceState == null) {
            savedInstanceState = new Bundle();
        }
        this.bundle = savedInstanceState;

        resultImageView = (ImageView) findViewById(R.id.resultImageView);

        captureQR();
    }


    public void captureQR() {
        Log.i("Capture", "Capturing QR...");

        Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
        startActivityForResult(intent, REQUEST_CAPTURE_QR);
    }

    public void captureResult() {
        Log.i("Capture", "Capturing results...");

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_CAPTURE_RESULTS);
        } else {
            Log.e("Capture", "No intent available to take picture");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            if (resultCode == RESULT_CANCELED) {
                Log.e("Capture", "Result was cancelled for requestCode " + requestCode);
            } else {
                Log.e("Capture", "Unable to get activity result for requestCode " + requestCode);
            }
            Toast.makeText(CaptureActivity.this, "Did not get OK result, code "
                    + resultCode + ", for request " + requestCode, Toast.LENGTH_LONG).show();
            return;
        }

        String result;
        final UUID learningObjectiveId, sequenceId;

        switch (requestCode) {

            case REQUEST_CAPTURE_QR:

                result = data.getStringExtra("SCAN_RESULT");
                Log.i("Capture", "Got QR code result " + result);
                learningObjectiveId = UUID.fromString(result.split(",")[0]);
                sequenceId = UUID.fromString(result.split(",")[1]);
                Log.i("Capture", "Learning objective id: " + learningObjectiveId.toString());
                Log.i("Capture", "Sequence id: " + sequenceId.toString());

                bundle.putString(KEY_LEARNING_OBJECTIVE_ID, learningObjectiveId.toString());
                bundle.putString(KEY_SEQUENCE_ID, sequenceId.toString());

                captureResult();
                break;

            case REQUEST_CAPTURE_RESULTS:

                Log.i("Capture", "Got Student result image");

                learningObjectiveId = UUID.fromString(bundle.getString(KEY_LEARNING_OBJECTIVE_ID));
                sequenceId = UUID.fromString(bundle.getString(KEY_SEQUENCE_ID));

                Bundle extras = data.getExtras();
                final Bitmap imageBitmap = (Bitmap) extras.get("data");
                resultImageView.setImageBitmap(imageBitmap);
                resultImageView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        Log.i("Capture", "Received click, uploading...");

                        new UploadTask(imageBitmap, learningObjectiveId, sequenceId).execute();
                    }
                });
                break;
            default:
        }
    }

    public class UploadTask extends AsyncTask<String, String, String> {

        private ProgressDialog progressDialog;

        private Bitmap bitmap;
        private UUID learningObjectiveId;
        private UUID sequenceId;

        public UploadTask(Bitmap bitmap, UUID learningObjectiveId, UUID sequenceId) {
            this.bitmap = bitmap;
            this.learningObjectiveId = learningObjectiveId;
            this.sequenceId = sequenceId;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(CaptureActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Uploading image to Light...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                executePost();
            } catch (Exception e ) {

            }
            return "";
        }

        @Overrid
        e
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            progressDialog.dismiss();
        }

        private void executePost() throws Exception {

            DefaultHttpClient httpClient = new DefaultHttpClient();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] data = baos.toByteArray();
            baos.close();


            BasicHttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, 30*1000);
            params.setParameter(KEY_LEARNING_OBJECTIVE_ID, learningObjectiveId);
            params.setParameter(KEY_SEQUENCE_ID, sequenceId);
            httpClient.setParams(params);

            String url = POST_UPLOAD_URL + "/" +
                    tilearningObjectiveId.toString() + "/" + sequenceId.toString();

            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(new ByteArrayEntity(data));

            HttpResponse httpResponse = httpClient.execute(httpPost);
            String response = EntityUtils.toString(httpResponse.getEntity());
            Log.i("Capture", "Got response " +
                    httpResponse.getStatusLine().getStatusCode() + " " + response);

        }

        public String getContent(HttpResponse response) throws IOException {
            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));
            String body = "";
            String content = "";

            while ((body = rd.readLine()) != null)
            {
                content += body + "\n";
            }
            return content.trim();
        }
    }
}
